package com.example.magicapp;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.magicapp.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView txtResposta;
    double fundo = 55;
    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        //esconde o maldito toolbar
        getSupportActionBar().hide();
    }

    public void MudarFundo() throws InterruptedException {
        double numero = Math.random() * 7;
        double valorAleatorio = Math.round(numero);
        View view;
        view = findViewById(R.id.principal);

        if (valorAleatorio == 1) { view.setBackgroundResource(R.drawable.fundoazul); }
        if (valorAleatorio == 2) { view.setBackgroundResource(R.drawable.fundoverde); }
        if (valorAleatorio == 3) { view.setBackgroundResource(R.drawable.fundovermelho); }
        if (valorAleatorio == 4) { view.setBackgroundResource(R.drawable.fundolaranja); }
        if (valorAleatorio == 5) { view.setBackgroundResource(R.drawable.fundomarrom); }
        if (valorAleatorio == 0) { view.setBackgroundResource(R.drawable.fundoverde); }
        if (valorAleatorio == 6) { view.setBackgroundResource(R.drawable.fundoroxo); }
        if (valorAleatorio == 7) { view.setBackgroundResource(R.drawable.fundoamarelo); }
    }

    public void GerarResposta(View view) throws InterruptedException {
        MudarFundo();
        txtResposta = (TextView) findViewById(R.id.tx_resposta);
        txtResposta.setText(String.format("Estou pensando..."));

        int x;

        final int[] i = {0};

        new Thread() {
            public void run() {
                while (i[0]++ < 100) {
                    try {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                double numero = Math.random() * 19;
                                double valorAleatorio = Math.round(numero);
                                String text = "";
                                if (valorAleatorio == 1) { text = "Certamente!"; }
                                if (valorAleatorio == 2) { text = "Sem dúvida!"; }
                                if (valorAleatorio == 3) { text = "Definitivamente sim!"; }
                                if (valorAleatorio == 4) { text = "Pode contar com isso!"; }
                                if (valorAleatorio == 5) { text = "Como eu vejo, sim!"; }
                                if (valorAleatorio == 0) { text = "Provavelmente!"; }
                                if (valorAleatorio == 6) { text = "Boa perspectiva!"; }
                                if (valorAleatorio == 7) { text = "Os sinais indicam que sim!"; }
                                if (valorAleatorio == 8) { text = "Sim!"; }
                                if (valorAleatorio == 9) { text = "Pergunte de novo mais tarde!"; }
                                if (valorAleatorio == 10) { text = "Pergunte novamente!"; }
                                if (valorAleatorio == 11) { text = "Melhor não lhe dizer agora!"; }
                                if (valorAleatorio == 12) { text = "Não posso prever agora!"; }
                                if (valorAleatorio == 13) { text = "Calma, pergunte novamente!"; }
                                if (valorAleatorio == 14) { text = "Não conte com isso!"; }
                                if (valorAleatorio == 15) { text = "Minha resposta é NÃO!"; }
                                if (valorAleatorio == 16) { text = "Minhas fontes dizem não!"; }
                                if (valorAleatorio == 17) { text = "A perspectiva não é boa!"; }
                                if (valorAleatorio == 18) { text = "Muito incerto!"; }

                                txtResposta.setText(String.format(text));
                                try {
                                    if (i[0] % 10 == 0) { MudarFundo(); };
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();


    }
}